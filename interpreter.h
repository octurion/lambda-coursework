#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <istream>

namespace interpreter
{
	void evaluate_input(std::istream&& input);
}

#endif
