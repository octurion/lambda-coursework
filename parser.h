#ifndef PARSER_H
#define PARSER_H

#include <algorithm>
#include <cstdint>
#include <exception>
#include <memory>
#include <sstream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "lexer.h"
#include "string_utils.h"

namespace parser
{
	using std::logic_error;
	using std::string;
	using std::tuple;
	using std::unique_ptr;
	using std::unordered_map;
	using std::vector;

	enum class ExpressionType
	{
		EMPTY,
		IDENTIFIER,
		APPLICATION,
		FUNCTION,
	};

	class Expression
	{
		private:
			ExpressionType expr_type;
			string identifier_str;
			unique_ptr<Expression> lhs_expr;
			unique_ptr<Expression> rhs_expr;

			Expression(ExpressionType type,
					string identifier,
					Expression&& lhs,
					Expression&& rhs)
				: expr_type(type)
				, identifier_str(std::move(identifier))
				, lhs_expr(lhs.type() == ExpressionType::EMPTY
						? nullptr
						: new Expression(std::move(lhs)))
				, rhs_expr(rhs.type() == ExpressionType::EMPTY
						? nullptr
						: new Expression(std::move(rhs)))
			{ }

		public:
			Expression()
				: expr_type(ExpressionType::EMPTY)
				, identifier_str()
				, lhs_expr(nullptr)
				, rhs_expr(nullptr)
			{ }

			explicit Expression(string identifier)
				: expr_type(ExpressionType::IDENTIFIER)
				, identifier_str(std::move(identifier))
			{ }

			Expression(Expression&& lhs, Expression&& rhs)
				: expr_type(ExpressionType::APPLICATION)
				, lhs_expr(new Expression(std::move(lhs)))
				, rhs_expr(new Expression(std::move(rhs)))
			{ }

			Expression(string parameter, Expression&& function_expr)
				: expr_type(ExpressionType::FUNCTION)
				, identifier_str(std::move(parameter))
				, lhs_expr(new Expression(std::move(function_expr)))
				, rhs_expr(nullptr)
			{ }

			// We don't want to use the copy ctor, because copying is expensive
			Expression deep_copy() const
			{
				Expression lhs = this->lhs_expr == nullptr
					? Expression()
					: this->lhs_expr->deep_copy();
				Expression rhs = this->rhs_expr == nullptr
					? Expression()
					: this->rhs_expr->deep_copy();

				return Expression(this->expr_type, this->identifier_str,
						std::move(lhs), std::move(rhs));
			}

			ExpressionType type() const { return this->expr_type; }

			const string& identifier() const
			{
				if (this->expr_type != ExpressionType::IDENTIFIER){
					throw logic_error("Expression must be an identifier");
				}
				return this->identifier_str;
			}

			const string& parameter() const
			{
				if (this->expr_type != ExpressionType::FUNCTION){
					throw logic_error("Expression must be a function");
				}
				return this->identifier_str;
			}

			string& parameter()
			{
				if (this->expr_type != ExpressionType::FUNCTION){
					throw logic_error("Expression must be a function");
				}
				return this->identifier_str;
			}

			Expression& lhs()
			{
				if (this->expr_type != ExpressionType::APPLICATION){
					throw logic_error("Expression must be an application");
				}
				return *this->lhs_expr;
			}

			const Expression& lhs() const
			{
				if (this->expr_type != ExpressionType::APPLICATION){
					throw logic_error("Expression must be an application");
				}
				return *this->lhs_expr;
			}

			Expression& rhs()
			{
				if (this->expr_type != ExpressionType::APPLICATION){
					throw logic_error("Expression must be an application");
				}
				return *this->rhs_expr;
			}

			const Expression& rhs() const
			{
				if (this->expr_type != ExpressionType::APPLICATION){
					throw logic_error("Expression must be an application");
				}
				return *this->rhs_expr;
			}

			Expression& body()
			{
				if (this->expr_type != ExpressionType::FUNCTION){
					throw logic_error("Expression must be a function");
				}
				return *this->lhs_expr;
			}

			const Expression& body() const
			{
				if (this->expr_type != ExpressionType::FUNCTION){
					throw logic_error("Expression must be a function");
				}
				return *this->lhs_expr;
			}

			string to_string() const
			{
				switch (this->expr_type){
				case ExpressionType::EMPTY: {
					return string();
				}

				case ExpressionType::IDENTIFIER: {
					return identifier_str;
				}

				case ExpressionType::APPLICATION: {
					const Expression* current = this;
					vector<string> strings;

					while (current->expr_type == ExpressionType::APPLICATION){
						const Expression& rhs = *current->rhs_expr;
						string rhs_string = current->rhs_expr->to_string();
						if (rhs.expr_type == ExpressionType::IDENTIFIER){
							strings.push_back(std::move(rhs_string));
						} else {
							strings.push_back(string_format("(%s)", rhs_string.c_str()));
						}
						current = current->lhs_expr.get();
					}

					string current_str = current->to_string();
					if (current->expr_type == ExpressionType::IDENTIFIER){
						strings.push_back(std::move(current_str));
					} else {
						strings.push_back(string_format("(%s)", current_str.c_str()));
					}
					std::reverse(strings.begin(), strings.end());

					std::ostringstream out;
					for (auto const& e : strings){
						if (&e != &strings[0]){
							out << " ";
						}
						out << e.c_str();
					}

					string str = out.str();
					return std::move(str);
				}

				case ExpressionType::FUNCTION: {
					return string_format("\\%s.%s",
							identifier_str.c_str(),
							lhs_expr->to_string().c_str());
				}

				default:
					return string();
				}
			}
	};

	typedef tuple<Expression, string> ParsingResult;

	ParsingResult parse_token_list(
			vector<lexer::Token> tokens,
			unordered_map<string, Expression>& aliases);
}

#endif
