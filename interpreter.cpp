#include "interpreter.h"
#include "parser.h"
#include "string_utils.h"

#include <cassert>
#include <iostream>
#include <istream>
#include <iterator>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <utility>

using std::cout;
using std::endl;
using std::string;
using std::getline;
using std::unordered_map;
using std::unordered_set;
using std::vector;

using parser::Expression;
using parser::ExpressionType;

namespace
{
	Expression* leftmost_beta_reduction(Expression& expr)
	{
		switch (expr.type()){
		case ExpressionType::EMPTY:
		case ExpressionType::IDENTIFIER:
			return nullptr;

		case ExpressionType::FUNCTION: {
			return leftmost_beta_reduction(expr.body());
		}

		case ExpressionType::APPLICATION: {
			if (expr.lhs().type() == ExpressionType::FUNCTION){
				return &expr;
			}

			Expression* left = leftmost_beta_reduction(expr.lhs());
			if (left != nullptr){
				return left;
			}

			return leftmost_beta_reduction(expr.rhs());
		}

		default:
			assert(false);
		}

		return nullptr;
	}

	bool is_free_variable(const string& variable, const Expression& expr)
	{
		switch (expr.type()){
		case ExpressionType::EMPTY:
			return true;

		case ExpressionType::IDENTIFIER:
			return expr.identifier() == variable;

		case ExpressionType::FUNCTION:
			if (expr.parameter() == variable){
				return false;
			}
			return is_free_variable(variable, expr.body());

		case ExpressionType::APPLICATION:
			return is_free_variable(variable, expr.lhs())
				|| is_free_variable(variable, expr.rhs());

		default:
			assert(false);
			return nullptr;
		}
	}

	void replace_with(Expression& tree,
			const string& parameter,
			const Expression& replacement,
			uintmax_t& new_var_id)
	{
		switch (tree.type()){
		case ExpressionType::EMPTY: {
			return;
		}	

		case ExpressionType::IDENTIFIER: {
			if (parameter == tree.identifier()){
				tree = replacement.deep_copy();
			}
			break;
		}

		case ExpressionType::APPLICATION: {
			replace_with(tree.lhs(), parameter, replacement, new_var_id);
			replace_with(tree.rhs(), parameter, replacement, new_var_id);
			break;
		}

		case ExpressionType::FUNCTION: {
			string& function_parameter = tree.parameter();
			Expression& body = tree.body();
			if (function_parameter == parameter){
				break;
			}

			bool must_substitute =
				   is_free_variable(function_parameter, replacement)
				&& is_free_variable(parameter, body);
			if (!must_substitute){
				replace_with(body, parameter, replacement, new_var_id);
			} else {
				string new_var = string_format("_var%ju", new_var_id++);
				replace_with(body, function_parameter, Expression(new_var), new_var_id);
				replace_with(body, parameter, replacement, new_var_id);

				std::swap(function_parameter, new_var);
			}

			break;
		}

		default:
			assert(false);
		}
	}

	void beta_reduce(Expression& expr, uintmax_t& new_var_id)
	{
		assert(expr.type() == ExpressionType::APPLICATION);
		Expression& func = expr.lhs();

		assert(func.type() == ExpressionType::FUNCTION);
		string parameter = func.parameter();
		Expression body = func.body().deep_copy();

		Expression& replacement = expr.rhs();

		replace_with(body, parameter, replacement, new_var_id);

		expr = std::move(body);
	}

	void eta_reduce(Expression& expr, uintmax_t& num_reductions)
	{
		switch (expr.type()){
		case ExpressionType::EMPTY:
		case ExpressionType::IDENTIFIER:
			return;

		case ExpressionType::APPLICATION:
			eta_reduce(expr.lhs(), num_reductions);
			eta_reduce(expr.rhs(), num_reductions);
			return;

		case ExpressionType::FUNCTION: {
			eta_reduce(expr.body(), num_reductions);

			Expression& body = expr.body();
			if (body.type() != ExpressionType::APPLICATION){
				return;
			}

			Expression& body_lhs = body.lhs();
			Expression& body_rhs = body.rhs();
			if (body_rhs.type() != ExpressionType::IDENTIFIER){
				return;
			}
			if (body_rhs.identifier() != expr.parameter()){
				return;
			}
			if (is_free_variable(body_rhs.identifier(), body_lhs)){
				return;
			}

			expr = body_lhs.deep_copy();
			num_reductions++;
			return;
		}

		default:
			assert(false);
		}
	}
}

void interpreter::evaluate_input(std::istream&& input)
{
	unordered_map<string, parser::Expression> aliases;
	uintmax_t line_num = 1;
	uintmax_t new_var_id = 0;

	for (string line; getline(input, line); ){
		lexer::LexerResult lex_result = lexer::tokenize_line(std::move(line),
				line_num++);

		vector<lexer::Token> tokens;
		vector<string> errors;
		std::tie(tokens, errors) = std::move(lex_result);

		if (!errors.empty()){
			for (auto const& err: errors){
				cout << err << endl;
			}
			break;
		}

		parser::ParsingResult res = parser::parse_token_list(
				std::move(tokens), aliases);

		string err;
		Expression unit;
		std::tie(unit, err) = std::move(res);
		if (!err.empty()){
			cout << err << endl;
			break;
		}

		if (unit.type() == ExpressionType::EMPTY){
			continue;
		}

		uintmax_t beta_reductions = 0;
		for (;;){
			Expression* leftmost_application = leftmost_beta_reduction(unit);
			if (leftmost_application == nullptr){
				break;
			}

			beta_reduce(*leftmost_application, new_var_id);
			beta_reductions++;
		}
		uintmax_t eta_reductions = 0;
		string test = unit.to_string();
		eta_reduce(unit, eta_reductions);
		string unit_str = unit.to_string();

		cout << string_format(
				"(%ju beta reductions & %ju eta reductions) Reduced to: %s",
				beta_reductions, eta_reductions, unit_str.c_str()) << endl;
	}
}
