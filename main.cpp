#include "interpreter.h"

#include <string>
#include <iostream>
#include <utility>

using std::cin;

int main()
{
	interpreter::evaluate_input(std::move(cin));

	return EXIT_SUCCESS;
}
