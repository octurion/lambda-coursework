#ifndef LEXER_H
#define LEXER_H

#include <cstdint>
#include <memory>
#include <string>
#include <stdexcept>
#include <tuple>
#include <vector>
#include <unordered_map>

namespace lexer
{
	using std::string;
	using std::vector;
	using std::invalid_argument;
	using std::tuple;

	enum class TokenType
	{
		BACKSLASH,
		DEFINED,
		DOT,
		LPAREN,
		RPAREN,
		IDENTIFIER,
		NUMBER,
		OPERATOR,
	};

	class Token
	{
		private:
			TokenType token_type;
			string token_contents;

			uintmax_t token_line;
			uintmax_t token_column;

			Token(TokenType token_type,
					string contents,
					uintmax_t line,
					uintmax_t column)
				: token_type(token_type)
				, token_contents(std::move(contents))
				, token_line(line)
				, token_column(column)
			{ }

		public:
			TokenType type() const { return this->token_type; }
			const string& contents() const { return this->token_contents; }
			uintmax_t line() const { return this->token_line; }
			uintmax_t column() const { return this->token_column; }

			static Token create_simple_token(TokenType type,
					uintmax_t line, uintmax_t column)
			{
				switch(type) {
				case TokenType::LPAREN:
				case TokenType::RPAREN:
				case TokenType::BACKSLASH:
				case TokenType::DOT:
				case TokenType::DEFINED:
					return Token(type, string(), line, column);

				default:
					throw invalid_argument(
						"Only parens, backslash or dot allowed as token types");
				}
			}

			static Token create_string_token(TokenType type,
					string contents,
					uintmax_t line,
					uintmax_t column)
			{
				switch(type) {
				case TokenType::OPERATOR:
				case TokenType::NUMBER:
				case TokenType::IDENTIFIER:
					return Token(type, std::move(contents), line, column);

				default:
					throw invalid_argument("Only operators, numbers and"
							" identifiers allowed as token types");
				}
			}
	};

	typedef tuple<vector<Token>, vector<string>> LexerResult;

	LexerResult tokenize_line(string line, uintmax_t line_num);
}

#endif
