#include "lexer.h"
#include "string_utils.h"

#include <algorithm>
#include <iterator>
#include <memory>
#include <unordered_set>

using lexer::TokenType;
using lexer::LexerResult;

using std::all_of;
using std::distance;
using std::make_tuple;
using std::string;
using std::unordered_map;
using std::unordered_set;
using std::vector;

namespace
{
	const unordered_map<char, lexer::TokenType> TOKEN_MAPPING =
	{
		{  '(', lexer::TokenType::LPAREN },
		{  ')', lexer::TokenType::RPAREN },
		{  '.', lexer::TokenType::DOT },
		{  '=', lexer::TokenType::DEFINED },
		{ '\\', lexer::TokenType::BACKSLASH },
	};

	const unordered_set<char> OPERATOR_CHARS =
	{
		'!', '@', '#', '$', '%', '^', '&', '*', '-', '+', '|', '<',
		'>', '/', '?', ':', ';', '~', '{', '}', '[', ']', '`', ','
	};

	bool is_whitespace(char ch)
	{
		return ch == ' ' || ch == '\t' || ch == '\n';
	}

	bool is_digit(char ch)
	{
		return '0' <= ch && ch <= '9';
	}

	bool is_letter(char ch)
	{
		return ('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z');
	}

	bool is_alphanumeric(char ch)
	{
		return is_digit(ch) || is_letter(ch);
	}

	bool is_operator(char ch)
	{
		return OPERATOR_CHARS.find(ch) != OPERATOR_CHARS.end();
	}
}

LexerResult lexer::tokenize_line(std::string line, uintmax_t line_num)
{
	vector<Token> accepted;
	vector<string> errors;

	auto iter = line.begin();
	auto end  = line.end();

	while (iter != end) {
		if (is_whitespace(*iter)){
			iter++;
			continue;
		}

		auto column = distance(line.begin(), iter) + 1;
		auto entry = TOKEN_MAPPING.find(*iter);
		if (entry != TOKEN_MAPPING.end()){
			accepted.push_back(Token::create_simple_token(
						entry->second, line_num, column));
			iter++;
			continue;
		}

		if (is_operator(*iter)){
			auto start = iter;
			while (iter != end && is_operator(*iter)){
				iter++;
			}

			accepted.push_back(Token::create_string_token(
						TokenType::OPERATOR,
						string(start, iter),
						line_num,
						column));
			continue;
		}

		if (is_alphanumeric(*iter)){
			auto start = iter;
			while (iter != end && is_alphanumeric(*iter)){
				iter++;
			}

			string identifier(start, iter);
			TokenType type = all_of(start, iter, is_digit)
				? TokenType::NUMBER
				: TokenType::IDENTIFIER;

			if (type == TokenType::IDENTIFIER && is_digit(*start)){
				errors.push_back(string_format(
						"In line %ju, col %ju: Identifier starts with a digit: %s",
						line_num, column, identifier.c_str()));
			}

			accepted.push_back(Token::create_string_token(
						type, std::move(identifier), line_num, column));
			continue;
		}

		errors.push_back(string_format(
					"In line %ju, col %ju: Not a valid character: %c",
					line_num, column, *iter));
		iter++;
	}

	return make_tuple(std::move(accepted), std::move(errors));
}
