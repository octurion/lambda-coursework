CC=g++
CFLAGS=-std=c++0x -Wall -Wextra -g3
OUT=lambda
OBJS=$(patsubst %.cpp, %.o, $(wildcard *.cpp))

.PHONY: all clean default

default: $(OUT)
all: default

$(OUT): $(OBJS)
	$(CC) $(OBJS) -o $(OUT)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $<

clean:
	-rm $(OBJS) $(OUT)
