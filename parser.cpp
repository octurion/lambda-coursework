#include "parser.h"
#include "string_utils.h"

#include <iostream>

#include <cassert>
#include <cstdint>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>

using lexer::Token;
using lexer::TokenType;

using parser::Expression;
using parser::ParsingResult;

using std::get;
using std::make_tuple;
using std::string;
using std::tuple;
using std::unordered_map;
using std::vector;

namespace {
	class ScopeHandle;
	class IdentifierScope
	{
		friend class ScopeHandle;

		private:
			unordered_map<string, uintmax_t> scoping_levels;

		public:
			ScopeHandle make_scope_object(string identifier);

			bool is_scoped(const string& identifier) const
			{
				return scoping_levels.find(identifier) != scoping_levels.end();
			}
	};

	class ScopeHandle
	{
		private:
			IdentifierScope& scope;
			string identifier;
			bool valid;

			// Only allow construction/destruction
			ScopeHandle();
			ScopeHandle(ScopeHandle& other) = delete;
			ScopeHandle& operator=(const ScopeHandle&) = delete;
			ScopeHandle& operator=(ScopeHandle&&) = delete;

		public:
			ScopeHandle(IdentifierScope& scope, string str)
				: scope(scope)
				, identifier(std::move(str))
				, valid(true)
			{
				auto& map = scope.scoping_levels;
				auto iter = map.find(this->identifier);
				if (iter != map.end()){
					iter->second++;
				} else {
					map[this->identifier] = 1;
				}
			}

			ScopeHandle(ScopeHandle&& other)
				: scope(other.scope)
				, identifier(std::move(other.identifier))
				, valid(true)
			{
				other.valid = false;
			}

			~ScopeHandle()
			{
				if (!this->valid){
					return;
				}

				auto& map = scope.scoping_levels;
				auto iter = map.find(this->identifier);
				iter->second--;
				if (iter->second == 0){
					map.erase(this->identifier);
				}
			}
	};

	ScopeHandle IdentifierScope::make_scope_object(string identifier)
	{
		return ScopeHandle(*this, std::move(identifier));
	}
}

namespace
{
	bool is_definition(const vector<lexer::Token>& tokens)
	{
		if (tokens.size() < 2){
			return false;
		}

		if (tokens[1].type() != TokenType::DEFINED){
			return false;
		}

		TokenType initial_type = tokens[0].type();
		return initial_type == TokenType::OPERATOR
			|| initial_type == TokenType::IDENTIFIER;
	}

	class ParsingState
	{
		private:
			vector<Token> tokens;
			string name_alias;
			std::vector<Token>::iterator current;

			const unordered_map<string, Expression>& aliases;
			IdentifierScope scope;

			bool is_combinator;

		public:
			ParsingState(vector<Token> parsed_tokens,
					const unordered_map<string, Expression>& expression_aliases)
				: tokens(std::move(parsed_tokens))
				, name_alias(is_definition(this->tokens)
						? tokens[0].contents()
						: string())
				, current(!name_alias.empty()
						? this->tokens.begin() + 2
						: this->tokens.begin())
				, aliases(expression_aliases)
				, scope()
				, is_combinator(!name_alias.empty())
			{ }

			bool is_still_combinator(){ return this->is_combinator; };
			void set_not_combinator(){ this->is_combinator = false; };
			const string& expression_alias(){ return this->name_alias; };
			ScopeHandle make_scope(string name)
			{
				return this->scope.make_scope_object(std::move(name));
			}

			const Token* peek()
			{
				if (current == tokens.end()){
					return nullptr;
				}

				return &*current;
			}

			bool has_ended()
			{
				return current == tokens.end();
			}

			const Expression* with_alias(const string& str)
			{
				auto iter = aliases.find(str);
				if (iter == aliases.cend()){
					return nullptr;
				}

				return &iter->second;
			}

			bool advance(){
				if (current == tokens.end()){
					return false;
				}

				current++;
				return true;
			}

			bool is_scoped(const string& name)
			{
				return scope.is_scoped(name);
			}
	};
}

namespace
{
	Expression create_church_numeral(uintmax_t value)
	{
		Expression body("x");
		for (uintmax_t i = 0; i < value; i++){
			body = Expression(Expression("f"), std::move(body));
		}

		return Expression("f", Expression("x", std::move(body)));
	}

	ParsingResult parsing_error(const Token& token, string message)
	{
		return make_tuple(Expression(), string_format("In line %ju, col %ju: %s",
				token.line(),
				token.column(),
				message.c_str()));
	}

	ParsingResult parsing_success(Expression&& expr)
	{
		return make_tuple(std::move(expr), string());
	}	

	ParsingResult parse_with_lookahead(ParsingState& state);
	ParsingResult parse_expression(ParsingState& state);
	ParsingResult parse_literal(ParsingState& state);
	ParsingResult parse_function(ParsingState& state);
	ParsingResult parse_parenthesized(ParsingState& state);

	ParsingResult parse_with_lookahead(ParsingState& state)
	{
		assert(!state.has_ended());

		const Token& token = *state.peek();
		switch (token.type()){
		case TokenType::BACKSLASH:
			return parse_function(state);

		case TokenType::IDENTIFIER:
		case TokenType::OPERATOR:
		case TokenType::NUMBER:
			return parse_literal(state);

		case TokenType::LPAREN:
			return parse_parenthesized(state);

		default:
			return parsing_error(token, "Expected left parenthesis,"
					" backslash, operator, number or identifier");
		}
	}

	ParsingResult parse_expression(ParsingState& state)
	{
		ParsingResult res = parse_with_lookahead(state);
		Expression head;
		std::tie(head, std::ignore) = std::move(res);
		if (!std::get<1>(res).empty()){
			return res;
		}

		while (!state.has_ended() && state.peek()->type() != TokenType::RPAREN){
			ParsingResult tail = parse_with_lookahead(state);
			if (!std::get<1>(tail).empty()){
				return tail;
			}

			Expression tail_expr;
			std::tie(tail_expr, std::ignore) = std::move(tail);

			head = Expression(std::move(head), std::move(tail_expr));
		}

		return parsing_success(std::move(head));
	}

	ParsingResult parse_parenthesized(ParsingState& state)
	{
		assert(!state.has_ended());
		const Token& lparen = *state.peek();
		assert(lparen.type() == TokenType::LPAREN);
		state.advance();

		if (state.has_ended()){
			return parsing_error(lparen,
					"Expected expression after left parenthesis");
		}

		Expression expr;

		ParsingResult res = parse_expression(state);
		if (!std::get<1>(res).empty()){
			return std::move(res);
		}

		std::tie(expr, std::ignore) = std::move(res);

		if (state.has_ended()){
			return parsing_error(lparen, "This left parenthesis is unmatched");
		}
		const Token& rparen = *state.peek();
		state.advance();
		if (rparen.type() != TokenType::RPAREN){
			return parsing_error(rparen,
					"Expected right parenthesis after expression");
		}

		return parsing_success(std::move(expr));
	}

	ParsingResult parse_literal(ParsingState& state)
	{
		assert(!state.has_ended());

		const Token& identifier = *state.peek();
		const string& name = identifier.contents();
		state.advance();

		const Expression* expr = state.with_alias(name);
		bool scoped = state.is_scoped(name);

		switch (identifier.type()){
		case TokenType::OPERATOR:
			if (expr == nullptr){
				string error = string_format(
						"No definition for operator %s", name.c_str());
				return parsing_error(identifier, std::move(error));
			}

			return parsing_success(expr->deep_copy());

		case TokenType::IDENTIFIER:
			if (expr != nullptr && !scoped){
				return parsing_success(expr->deep_copy());
			}

			if (!scoped){
				state.set_not_combinator();
			}

			return parsing_success(Expression(std::move(name)));

		case TokenType::NUMBER:
			return parsing_success(create_church_numeral(std::stoull(name)));

		default:
			assert(false); // Should be unreachable
		}

		return make_tuple(Expression(), string()); // Dead code
	}

	ParsingResult parse_function(ParsingState& state)
	{
		assert(!state.has_ended());

		const Token& backslash = *state.peek();
		// We should've already done a lookahead
		assert(backslash.type() == TokenType::BACKSLASH);
		state.advance();

		if (state.has_ended()){
			return parsing_error(backslash,
					"Got end-of-line instead of an identifier token");
		}
		const Token& identifier = *state.peek();
		if (identifier.type() != TokenType::IDENTIFIER){
			return parsing_error(identifier,
					"Expected an identifier token after backslash");
		}
		const string& name = identifier.contents();
		state.advance();

		// When returning, the scope will be automatically released
		ScopeHandle parameter_scope = state.make_scope(name);

		if (state.has_ended()){
			return parsing_error(backslash, "Got end-of-line instead of a dot");
		}
		const Token& dot = *state.peek();
		state.advance();
		if (dot.type() != TokenType::DOT){
			return parsing_error(dot, "Expected a dot after an identifier");
		}

		ParsingResult res = parse_expression(state);
		if (!std::get<1>(res).empty()){
			return res;
		}

		Expression body;
		std::tie(body, std::ignore) = std::move(res);

		return parsing_success(Expression(name, std::move(body)));
	}
}

parser::ParsingResult parser::parse_token_list(
		vector<lexer::Token> tokens,
		unordered_map<string, Expression>& aliases)
{
	ParsingState state(std::move(tokens), aliases);
	if (state.peek() == nullptr){
		return parsing_success(Expression());
	}

	ParsingResult res = parse_expression(state);
	if (!std::get<1>(res).empty()){
		return res;
	}

	const Token* remaining = state.peek();
	if (remaining != nullptr){
		return parsing_error(
				*remaining,
				"Unnecessary tokens found from here to the end of the line");
	}

	const string& alias = state.expression_alias();
	if (!state.is_still_combinator() && !alias.empty()){
		return make_tuple(Expression(),
			string_format("Alias '%s' must be a combinator", alias.c_str()));
	}

	if (state.is_still_combinator()){
		auto iter = aliases.find(alias);
		if (iter != aliases.end()){
			return make_tuple(Expression(), string_format(
						"Alias %s has been already defined",
						alias.c_str()));
		}

		Expression expr;
		std::tie(expr, std::ignore) = std::move(res);
		aliases.insert(std::make_pair(alias, std::move(expr)));

		return parsing_success(Expression());
	} else {
		return res;
	}
}
