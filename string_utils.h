#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <memory>
#include <iostream>
#include <string>
#include <cstdio>
#include <utility>

inline std::string string_format(std::string format)
{
	return std::move(format);
}

template<typename ... Args> inline
std::string string_format(const std::string& format, Args ... args)
{
    size_t size = 1 + std::snprintf(nullptr, 0, format.c_str(), args ...);

	std::unique_ptr<char[]> buf(new char[size]);
	std::snprintf(buf.get(), size, format.c_str(), args ...);

    return std::string(buf.get(), buf.get() + size);
}

#endif
